ARG base_image=ubuntu:20.04
FROM $base_image

RUN apt-get update \
	&& apt-get install -y  --no-install-recommends curl ca-certificates \
	&& apt-get -y autoremove \
	&& rm -rf /var/lib/apt/lists/*

ARG node_version=12

RUN echo "node version: $node_version" && curl -fsSL https://deb.nodesource.com/setup_$node_version.x | bash - \
    && apt-get install -y nodejs

COPY . /utils
RUN cd /utils && npm install

EXPOSE 5672

ARG queue
ARG progress="False"
ENV QUEUE $queue
ENV CS_PROGRESS $progress

ARG timeout
CMD /utils/wait-for-it.sh rabbitmq:5672 --timeout=${timeout:-30} -- node /utils/index.js $QUEUE

