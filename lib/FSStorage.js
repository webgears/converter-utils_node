const fs = require('fs')
const util = require('util');
const pth = require('path');
const BaseStorage = require('./BaseStorage');

class FSStorage extends BaseStorage {

    constructor(storagePath) {
        super();
        this.storagePath = storagePath;
    }

    readFile(file, path) {
        const fsPath = `${this.storagePath}/${file}`;
        return new Promise((onResolve, onReject) => {
            util.promisify(fs.mkdir)(pth.dirname(path), {recursive: true})
                .then(() => {
                    fs.createReadStream(file)
                    .pipe(
                        fs.createWriteStream(path)
                            .on('finish', () => onResolve(path))
                            .on('error', (err) => onReject(err))
                        )
                    .on('error', (err) => onReject(err))
                })
                .catch((err) => onReject(err));
        });
    }

    isFolder(path) {
        return util.promisify(fs.stat)(path).then((stats) => stats.isDirectory())
    }

    async contents(dir) {
        const result = [];
        const files = await util.promisify(fs.readdir)(dir, {withFileTypes: true})
        await Promise.all(files.map(async (file) => {
            if (file.isDirectory()) {
                const children = await this.contents(`${dir}/${file.name}`);
                result.push(...children);
            } else {
                result.push(pth.relative(this.storagePath, `${dir}/${file.name}`));
            }
        }));
        return result;
    }

    readDirContents(dir, path) {
        return this.contents(`${this.storagePath}/${dir}`)
            .then(files => Promise.all(files.map(file => this.readFile(`${this.storagePath}/${file}`, `${path}/${file}`))))
    }

    write(path, readStream) {
        const fsPath = `${this.storagePath}/${path}`;
        return new Promise((onResolve, onReject) => {
            util.promisify(fs.mkdir)(pth.dirname(fsPath), {recursive: true})
                .then(() => {
                    readStream
                    .pipe(
                        fs.createWriteStream(fsPath)
                            .on('finish', () => onResolve(path))
                            .on('error', (err) => onReject(err))
                        )
                    .on('error', (err) => onReject(err))
                })
                .catch((err) => {
                    throw(err)
                })
        });
    }

    list(key) {
        throw('method list should be implemented in subclass');
    }

    delete(key) {
        throw('method delete should be implemented in subclass');
    }
}

module.exports = FSStorage;