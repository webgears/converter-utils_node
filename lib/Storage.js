const FSStorage = require('./FSStorage');
const S3Storage = require('./S3Storage');

const fsStoragePath = '/var/app/public/conversions';

class Storage {

    constructor() {
        this._storage = null;
    }

    create() {
        switch (process.env.storage) {
            case 's3':
                this._storage = new S3Storage();
                break;
            case 'fs':
                this._storage = new FSStorage(fsStoragePath);
                break;
            default:
                throw ('no storage type was provided in env variable')
        }

    }

    get instance() {
        if (!this._storage) {
            this.create()
        }
        return this._storage;
    }

}

const StorageInstance = new Storage();

module.exports = StorageInstance;