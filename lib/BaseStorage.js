class BaseStorage {
    constructor() {

    }

    read() {
        throw('method read should be implemented in subclass');
    }

    write() {
        throw('method write should be implemented in subclass');
    }

    list() {
        throw('method list should be implemented in subclass');
    }

    delete() {
        throw('method delete should be implemented in subclass');
    }
}

module.exports = BaseStorage;