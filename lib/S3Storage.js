const Minio = require('minio')
const util = require('util');
const fs = require('fs');
const pth = require('path');

const BaseStorage = require('./BaseStorage');

const bucket = 'wg-bucket';

const minioConfig = {
    endPoint: process.env.MINIO_END_POINT,
    port: parseInt(process.env.MINIO_PORT),
    useSSL: process.env.MINIO_USE_SSL === 'true',
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY
}

module.exports.init = async () => {    
    try {
        const bucketExists = await minioClient.bucketExists(bucket);
        if (!bucketExists) {
            await minioClient.makeBucket(bucket);
            console.log(`Bucket ${bucket} created successfully.`)
        } else {
            console.log(`Bucket ${bucket} already exists.`)
        }
    } catch (err) {
        if (err) throw(err)
    }
    
}

class S3Storage extends BaseStorage {
    constructor() {
        super();
        this.client = new Minio.Client(minioConfig);
    }

    ensureBucket() {
        return this.client.bucketExists(bucket)
            .then((bucketExists) => {
                if (!bucketExists) {
                    return minioClient.makeBucket(bucket);
                } else {
                    return Promise.resolve()
                }
            })
            .then(() => {
                return Promise.resolve();
            })
            .catch((err) => {
                throw(err)
            })
    }
    
    isFolder(path) {
        const contents = [];
        return new Promise((onResolve, onReject) => {
            const stream = this.client.listObjects(bucket, path, true);
            stream.on('data', (object) => { 
                contents.push(object.name) 
            })
            stream.on('end', () => onResolve(contents));
            stream.on('error', (err) => onReject(err))
        }).then((result) => {
            if (result.length > 1) return true;
            if (result.length === 1 && !result[0].endsWith(path)) return true;
            return false;
        })
    }

    readFile(file, path) {
        return new Promise((onResolve, onReject) => {
            util.promisify(fs.mkdir)(pth.dirname(path), {recursive: true})
                .then(() => this.client.getObject(bucket, file)
                .then((dataStream) => {
                    dataStream.pipe(
                        fs.createWriteStream(path)
                            .on('finish', () => onResolve(path))
                            .on('error', (err) => onReject(err))
                        )
                        .on('error', (err) => onReject(err))
                })
                .catch((err) => onReject(err)));
        });
    }

    readDirContents(dir, path)  {
        return this.contents(dir)
            .then(files => Promise.all(files.map(file => this.readFile(file, `${path}/${file}`))))
    }

    contents(dir) {
        const contents = [];
        return new Promise((onResolve, onReject) => {
            const stream = this.client.listObjects(bucket, dir, true);
            stream.on('data', (object) => { 
                contents.push(object.name) 
            })
            stream.on('end', () => onResolve(contents));
            stream.on('error', (err) => onReject(err))
        })
    }

    write(path, stream) {
        const sanitizedPath = path.replace(/^\/+/g, '')
        return this.ensureBucket()
            .then(() => this.client.putObject(bucket, sanitizedPath, stream))
    }

    list(key) {
        throw('method list should be implemented in subclass');
    }

    delete(key) {
        throw('method delete should be implemented in subclass');
    }

}

module.exports = S3Storage;