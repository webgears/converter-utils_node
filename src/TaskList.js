class TaskList {

    constructor() {
        this._tasks = {};
    }

    add(id) {
        this._tasks[id] = {
            status: TaskList.STATUSES.pending
        };
        console.log(`received task ${id}`);
    };

    _change(id, value) {
        if (!this._tasks[id]) {
            throw(`no task with id ${id}`);
        }
        this._tasks[id] = Object.assign(this._tasks[id], value);
        this.onChange && this.onChange();
    }

    onChange() {};

    isInList(id) {
        return !!this._tasks[id];
    }

    isFinished(id) {
        if (!this._tasks[id]) {
            throw(`no task with id ${id}`);
        }
        return this._tasks[id].status === TaskList.STATUSES.finished;
    };

    isFinishedWithErrors(id) {
        if (!this._tasks[id]) {
            throw(`no task with id ${id}`);
        }
        return this._tasks[id].status === TaskList.STATUSES.error;
    }

    isInProcess(id) {
        if (!this._tasks[id]) {
            throw(`no task with id ${id}`);
        }
        return this._tasks[id].status === TaskList.STATUSES.inProcess;
    };

    get isNoneInProcess() {
        return !Object.values(this._tasks).some((task) => {
            return task.status === TaskList.STATUSES.inProcess
        });
    }

    getPid(id) {
        if (!this._tasks[id]) {
            throw(`no task with id ${id}`);
        }
        return this._tasks[id].pid;
    }

    setInProcess(id, pid) {
        this._change(id, {
            status: TaskList.STATUSES.inProcess,
            pid
        });
        console.log(`spawned process ${pid} for task ${id}`);
    };

    setFinished(id) {
        this._change(id, {
            status: TaskList.STATUSES.finished,
        });
        console.log(`finished task ${id}`);
    };

    setCanceled(id) {
        this._change(id, {
            status: TaskList.STATUSES.canceled,
        });
        console.log(`canceled task ${id}`);
    }

    setError(id) {
        this._change(id, {
            status: TaskList.STATUSES.error,
        });
        console.log(`task ${id} finished with error`);
    }

    remove(id) {
        console.warn('should not be used');
        delete this._tasks[id];
    };

    clear() {
        this._tasks = {};
    };
}

TaskList.STATUSES = {
    pending: 'pending',
    inProcess: 'inProcess',
    finished: 'finished',
    canceled: 'canceled',
    error: 'error'
};

module.exports = TaskList;
