const fs = require('fs')
const util = require('util');
const execFile = util.promisify(require('child_process').execFile);

const sleep = (ms = 0) => {
    return new Promise(r => setTimeout(r, ms));
};

const getCmd = (body) => {
    return JSON.parse(body)['cmd']
};

const getTaskId = (cid) => {
    const [_input, _jobId, taskId] = cid.match(/jobId=(.+)&taskId=(.+)/);
    return taskId;
};

const formatProgressMessage = (progress) => {
    const message = {
        progress
    };
    return message;
};

const formatFinishMessage = ({code, signal, stdout, stderr}) => {
    const message = {
        code: code,
        signal: signal,
        stdout: stdout,
        stderr: stderr
    };
    return message;
};

const isProcessCanceled = (code, signal) => {
    return code === 15 || signal === 'SIGTERM'
};

const isProcessFinishedWithError = (code, signal) => {
    return code !== 0 || !!signal
};

const isProcessRunning = async (pid) => {
    const { stdout } = await execFile('ps', ['-o', 'pid=']);
    const pids = stdout.match(/\d+/gi);
    return pids.map(pid => Number(pid)).includes(pid);
};

const killProcess = async (pid) => {
    process.kill(pid, 'SIGKILL');
};

const cancelProcess = async (pid) => {
    process.kill(pid, 'SIGTERM');
};

const progressRegexp = /\{\{(.+)\}\}/;

const isProgressString = (str) => {
    return progressRegexp.test(str);
};

//todo: validate progress
const extractProgress = (str) => {
    return isProgressString(str)
        ? str.match(progressRegexp)[1]
        : null
};

const readDirRecursive = (dir) => {
    const result = [];
    const files = fs.readdirSync(dir, {withFileTypes: true});
    files.forEach(file => {
        if (file.isDirectory()) {
            result.push(...readDirRecursive(`${dir}/${file.name}`));
        } else {
            result.push(`${dir}/${file.name}`);
        }
    });
    return result;
};

const mapArray = (item, fn) => {
    if (Array.isArray(item)) {
        return item.map((arrItem) => fn(arrItem))
    }
    return fn(item);
}

const asyncReduce = async (array, handler, startingValue) => {
    let result = startingValue;
    for (value of array) {
      result = await handler(result, value);
    }
    return result;
}

const asyncMapArray = (item, fn) => {
    if (Array.isArray(item)) {
        return Promise.all(item.map((arrItem) => fn(arrItem)))
    }
    return Promise.resolve(fn(item));
}

const forEachArray = (item, fn) => {
    if (Array.isArray(item)) {
        item.forEach((arrItem) => fn(arrItem))
    }
    return fn(item);
}

const extLinkRegExp = /^(http|https):\/\/[^ "]+\.[^ "]+$/;

const isLink = (path) => {
    return (extLinkRegExp).test(path)
};

module.exports = {
    sleep,
    getCmd,
    getTaskId,
    formatFinishMessage,
    formatProgressMessage,
    isProcessCanceled,
    isProcessRunning,
    isProcessFinishedWithError,
    killProcess,
    cancelProcess,
    isProgressString,
    extractProgress,
    readDirRecursive,
    mapArray,
    asyncMapArray,
    forEachArray,
    asyncReduce,
    isLink
};
