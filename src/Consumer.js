class Consumer {
    constructor(channel, queue) {
        this.channel = channel;
        this.queue = queue;
    }

    reject(message) {
        this.channel.nack(message, false, true);
    }

    confirm(message) {
        this.channel.ack(message);
    }

    respond(replyTo, correlationId, message) {
        this.channel.sendToQueue(replyTo, message, {
            correlationId: correlationId
        });
    };

    async consume(onConsume) {
        await this.channel.addSetup((channel) =>
            channel.consume(this.queue, (message) => {
                const respond = this.respond.bind(
                    this,
                    message.properties.replyTo,
                    message.properties.correlationId
                );
                onConsume(message, {
                    confirm: this.confirm.bind(this),
                    reject: this.reject.bind(this),
                    respond
                });
            })
        )
    }

    async get() {
        const message = await this.channel.get(this.queue);
        const respond = message
            ? this.respond.bind(
                this,
                message.properties.replyTo,
                message.properties.correlationId
            )
            : () => {};
        return {
            message,
            confirm: this.confirm.bind(this),
            reject: this.reject.bind(this),
            respond
        };
    }
}

module.exports = Consumer;
