const fs = require('fs');
const path = require('path')
const cmdFn = require('../converter/cmd.js');
const storage = require('../lib/Storage').instance;
const { 
    readDirRecursive,
    asyncMapArray,
    mapArray,
    asyncReduce,
    isLink
 } = require('./utils');

const localDir = '/conversions';

fs.mkdirSync(localDir, {recursive: true});

class UtilsStorage {
    constructor(message) {
        const {input, output, rootDir, jobDir, actorDir, download} = JSON.parse(message.content.toString());
        this.input = input;
        this.output = output;
        this.outputDir = rootDir;
        this.jobDir = jobDir;
        this.actorDir = actorDir;
        this.download = download;
        this.localInput = {};
    }

    buildOutputPath(filename) {
        fs.mkdirSync(path.dirname(`${localDir}/${filename}`), {recursive: true});
        return `${localDir}/${filename}`
    };

    async buildCmd() {
        if (this.download) {
            await storage.readDirContents(this.download, localDir)
        }

        const localInputFiles = await asyncReduce(Object.entries(this.input.files), 
            (async (acc, [name, file]) => {
                acc[name] = await asyncMapArray(file, (f) => {
                    if (isLink(f)) return f;
                    const fsPath = f.replace(`${this.outputDir}/`, '');
                    return storage.isFolder(f).then(isFolder => {
                        if (isFolder) {
                            return storage.readDirContents(fsPath, localDir).then(() => encodeURI(`${localDir}/${fsPath}`));
                        } else {
                            return storage.readFile(f, encodeURI(`${localDir}/${fsPath}`))
                        }
                    })
                    
                });
                return acc;
            }),
            {}
        );

        const localOutputFiles = Object.entries(this.output.files)
            .reduce((acc, [name, file]) => {
                acc[name] = mapArray(file, (f) => this.buildOutputPath(f.replace(`${this.outputDir}/`, '')));
                return acc;
            }, {}
        );

        return this.cmd({...this.input.options, ...localInputFiles, ...localOutputFiles})
    }

    writeResults() {
        const outputFiles = readDirRecursive(`${localDir}/${this.actorDir}`);
        return Promise.all(outputFiles.map((file) => {
            const newPath = file.replace(`${localDir}/`, '')
            return storage.write(newPath, fs.createReadStream(file))
        }))
    }

    cmd(args) {
        if (!cmdFn) {
            throw('no cmd was provided by converter')
        }
        return cmdFn(args);
    }

}

module.exports = UtilsStorage;