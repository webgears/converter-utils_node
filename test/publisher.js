const amqp = require('amqplib');
const rabbitmqHost = 'amqp://localhost:5672';

const createChannel = async () => {
    const connection = await amqp.connect(rabbitmqHost);
    process.once('SIGINT', function() { connection.close(); });

    const channel = await connection.createChannel();
    return channel;
};

const sendTestMessage = async () => {
    const channel = await createChannel();

    channel.sendToQueue('test', Buffer.from('test string', 'utf-8'));
};

sendTestMessage();