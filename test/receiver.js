const amqp = require('amqplib');

const consume = async () => {
    const conn = await amqp.connect('amqp://localhost:5672');
    process.once('SIGINT', function() { conn.close(); });
    const ch = await conn.createChannel();

    const ok = await ch.assertQueue('test', {durable: false});
    ch.consume(ok.queue, reply => {
        console.log(reply.content.toString());
    }, {noAck: true});
    console.log(' [*] Waiting for logs. To exit press CTRL+C');
};

consume().catch(err => console.log(err));