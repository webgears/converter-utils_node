const fs = require('fs');
const path = require('path');
const childProcess = require('child_process');
//const amqp = require('amqplib');
const amqp = require('amqp-connection-manager');
const split = require('split');
const UtilsStorage = require('./src/UtilsStorage');

const Consumer = require('./src/Consumer');
const TaskList = require('./src/TaskList');
const {
    sleep,
    getTaskId,
    formatFinishMessage,
    formatProgressMessage,
    isProcessFinishedWithError,
    isProcessCanceled,
    isProcessRunning,
    killProcess,
    cancelProcess,
    isProgressString,
    extractProgress
} = require('./src/utils');

const spawnProcess = (cmd, onFinish, onProgress) => {
    const [command, ...args] = cmd.split(' ');
    const process = childProcess.spawn(command, args.map(a => decodeURI(a)));

    let stdout = '';
    process.stdout
        .pipe(split())
        .on('data', (data) => {
            const str = data.toString();
            if (isProgressString(str)) {
                onProgress(extractProgress(str))
            }
            stdout += `${data}\n`;
        });

    let stderr = '';
    process.stderr.on('data', (data) => {
        console.log(data.toString());
        stderr += data;
    });

    process.on('close', (code, signal) => {
        console.log('process closing...');
        stdout = stdout.trim();
        onFinish({ code, signal, stdout, stderr });
    });

    return process.pid;
};

const waitForConversionMessage = async (consumer, taskList) => {
    console.log('waiting for messages...');
    while (taskList.isNoneInProcess) {
        const {message, respond, confirm} = await consumer.get();

        if (message) {
            console.log('received message');
            const taskId = getTaskId(message.properties.correlationId);
            taskList.add(taskId);

            const utilsStorage = new UtilsStorage(message);
            try {
                const cmd = await utilsStorage.buildCmd();

                console.log(`cmd ${cmd}`);

                const pid = spawnProcess(
                    cmd,
                    async (processOutput) => {
                        const { code, signal } = processOutput;
                        console.log(`process closed with code ${code} and signal ${signal}`);
    
                        if (isProcessCanceled(code, signal)) {
                            taskList.setCanceled(taskId);
                        } else if (isProcessFinishedWithError(code, signal)) {
                            taskList.setError(taskId);
                        } else {
                            taskList.setFinished(taskId);
                            try {
                                await utilsStorage.writeResults();    
                            } catch (err) {
                                throw('error writing conversion results to storage', err);
                            }
                        }
                        respond(formatFinishMessage(processOutput));
                    },
                    (progress) => {
                        console.log(`progress sent`);
                        respond(formatProgressMessage(progress));
                    }
                );
    
                taskList.setInProcess(taskId, pid);
                confirm(message);
            } catch (err) {
                throw('error reading input from storage', err);
            }
           
        }
        await sleep(100);
    }
};

const TIMEOUT = 1000 * 60 * 1; //1 minute

const ensureProcessClosed = async (pid) => {
    const endtime = Date.now() + TIMEOUT;
    while (await isProcessRunning(pid)) {
        if (Date.now() >= endtime) {
            killProcess(pid);
            break;
        }
        await sleep(100);
    }
};

const cancelConsume = (cancelConsumer, taskList) => {
    return cancelConsumer.consume(async (message, {confirm, reject, respond}) => {
        const taskId = getTaskId(message.properties.correlationId);

        if (!taskList.isInList(taskId)) {
            console.log('task is not in list');
            reject(message);
            return;
        }

        if (taskList.isFinished(taskId) || taskList.isFinishedWithErrors(taskId)) {
            console.log(`task ${taskId} already finished`);
            confirm(message);
            return;
        }

        if (taskList.isInProcess(taskId)) {
            const pid = taskList.getPid(taskId);
            console.log('task is in process', pid);
            cancelProcess(pid);
            await ensureProcessClosed(pid);
            confirm(message);
        }
    });
};

const waitForPing = (channel, exchange, channelWrapper, oneoffQueue, fn = () => {}) => {
    console.log('waiting for ping');

    channel.bindQueue(oneoffQueue.queue, exchange, '');
    channel.consume(oneoffQueue.queue, (msg) => {
        if (msg.content) {
            fn && fn(channelWrapper);
        }
    })
}

const rabbitmqHost = 'amqp://rabbitmq:5672';

if (!process.argv[2]) {
    throw 'no queue name provided';
}

const conversionQueueName = process.argv[2];
const cancelQueueName = `${conversionQueueName}_cancel`;
const registerQueueName = 'register';
const pingExchangeName = 'ping';

let i = 0

const register = (channelWrapper) => {
    const config = require('./converter/config.json');
    try {
        config['version'] = fs.readFileSync(path.join(__dirname, './converter/VERSION'), {encoding: 'utf8'});
    } catch (err) {
        console.log('error reading VERSION file: ', err);
    }
    console.log('send to queue to register', i++)
    return channelWrapper
        .sendToQueue('register', config, {})
        .catch(err => console.log(err));
}

const main = async () => {
    const connection = await amqp.connect(rabbitmqHost);
    process.once('SIGINT', function() { connection.close(); });
    const taskList = new TaskList();
    const channelWrapper = connection.createChannel( {
        json: true,
        setup: async function(channel) {
            console.log('converter setup');
            await channel.assertQueue(conversionQueueName);
            await channel.assertQueue(cancelQueueName);
            await channel.assertQueue(registerQueueName);
            await channel.assertExchange(pingExchangeName, 'fanout');
            const oneoffQueue = await channel.assertQueue('', {
                exclusive: true
            });
            console.log('asserted queue');
            //console.log('asserted exchange');
            // const cancelConsumer = new Consumer(channelWrapper, cancelQueueName);
            const conversionConsumer = new Consumer(channelWrapper, conversionQueueName);
            const cancelConsumer = new Consumer(channelWrapper, cancelQueueName);
            cancelConsume(cancelConsumer, taskList);
            waitForConversionMessage(conversionConsumer, taskList);
            waitForPing(channel, pingExchangeName, this, oneoffQueue, register)
            // await cancelConsume(cancelConsumer, taskList);
            // await waitForConversionMessage(conversionConsumer, taskList);
        }
    } );

    await register(channelWrapper);

    const conversionConsumer = new Consumer(channelWrapper, conversionQueueName);
    const cancelConsumer = new Consumer(channelWrapper, cancelQueueName);
    //const taskList = new TaskList();

    taskList.onChange = async () => {
        if (taskList.isNoneInProcess) {
            try {
                await waitForConversionMessage(conversionConsumer, taskList)
            } catch (err) {
                console.log(err, 'error consuming messages');
                process.exitCode = 2;
            }
        }
    };

    // try {
    //     register();
    //     await waitForPing(channel, pingExchange.exchange, register)
    //     await cancelConsume(cancelConsumer, taskList);
    //     await waitForConversionMessage(conversionConsumer, taskList);
    // } catch (err) {
    //     console.log(err, 'error consuming messages');
    //     process.exitCode = 2;
    // }
};

main()
    .catch(err => {
        console.log(err, 'error establishing connection with rabbit')
        process.exitCode = 1;
    });

['SIGINT', 'SIGTERM'].forEach(function(signal) {
    process.on(signal, function() {
        console.log('Closing container');
        process.exitCode = 3;
    });
});
